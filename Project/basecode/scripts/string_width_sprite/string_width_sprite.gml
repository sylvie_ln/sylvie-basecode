///@param text The text.
// Measures width of text with mid-text sprites. 
// Limitations: doesn't do linebreakcs properly.
return sprite_text_impl(0,0,argument[0],sprite_text_modes.width);