var xp = argument[0];
var yp = argument[1];
var list = string_split(argument[2],"\n");
var h = string_height(argument[2]);
var va = draw_get_valign();
if va == fa_middle {
	yp -= h div 2;	
} else if va == fa_bottom {
	yp -= h;
}
var yoff = 0;
for(var i=0; i<ds_list_size(list); i++) {
	var text = list[|i];
	var tw = string_width(text);
	var th = string_height(text);
	draw_text(xp-(tw div 2),yoff+yp,text);
	yoff += th;
}
ds_list_destroy(list);