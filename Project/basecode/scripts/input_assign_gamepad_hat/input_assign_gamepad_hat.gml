///@description Assigns a gamepad hat input to the given action.
///@param action_name String representing the action name.
///@param [hat The hat to assign to the action.
///@param dir]... The direction of the hat.
var action = argument[0];
var list = oInput.actions[? action];
for(var i=1; i<argument_count; i+=2) {
	var hat = argument[i];
	var dir = argument[i+1];
	var was_waiting = false;
	for(var j=0; j<ds_list_size(list); j++) {
		var input = list[|j];
		if input[|0] == input_kind.waiting {
			input[|0] = input_kind.gamepad_hat;
			ds_list_add(input,hat);
			ds_list_add(input,dir);
			was_waiting = true; 
			break;
		}
	}
	if !was_waiting {
		ds_list_add(list,list_create(input_kind.gamepad_hat,hat,dir));
	}
}