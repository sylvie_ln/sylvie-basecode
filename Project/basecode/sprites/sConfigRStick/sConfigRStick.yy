{
    "id": "0480b942-ba32-41b1-8bea-438c29d227da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigRStick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6209bf60-624a-48c1-80bd-0c7397a695c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0480b942-ba32-41b1-8bea-438c29d227da",
            "compositeImage": {
                "id": "d50cb248-f524-4375-aeae-82fc00c83bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6209bf60-624a-48c1-80bd-0c7397a695c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e04187-04db-4fbe-ba2a-39233efc4445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6209bf60-624a-48c1-80bd-0c7397a695c3",
                    "LayerId": "1d0d0758-a3ae-4c96-9a72-930fcabef31c"
                },
                {
                    "id": "ae6f943e-a7ec-4a25-a55f-28e9e39e5f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6209bf60-624a-48c1-80bd-0c7397a695c3",
                    "LayerId": "3b7807bc-0919-459c-8484-4d5ec630dcbd"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 14,
    "layers": [
        {
            "id": "1d0d0758-a3ae-4c96-9a72-930fcabef31c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0480b942-ba32-41b1-8bea-438c29d227da",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3b7807bc-0919-459c-8484-4d5ec630dcbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0480b942-ba32-41b1-8bea-438c29d227da",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}