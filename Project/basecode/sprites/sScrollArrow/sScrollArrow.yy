{
    "id": "2e007fd3-497e-4d23-a09e-d13833c3a5d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScrollArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21676155-839c-4f60-bc68-83f2768ecae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e007fd3-497e-4d23-a09e-d13833c3a5d6",
            "compositeImage": {
                "id": "1bba1d3b-d68f-4688-9cdb-09c72b5e9452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21676155-839c-4f60-bc68-83f2768ecae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13491c9e-1c4b-4401-a429-7e830e063ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21676155-839c-4f60-bc68-83f2768ecae4",
                    "LayerId": "12f9dd96-2b52-4dcb-b390-9fe3fdf1c76a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "12f9dd96-2b52-4dcb-b390-9fe3fdf1c76a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e007fd3-497e-4d23-a09e-d13833c3a5d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 0
}