{
    "id": "3deded34-d8e9-42a6-ae39-ddf667042894",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSylvieFontB2x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea6a60ec-53f5-40ea-8a65-6add6066a46e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5dcba6f2-e5cd-488e-a5e6-cb5db49a4fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6a60ec-53f5-40ea-8a65-6add6066a46e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "252b8fc6-ec2e-40eb-81b1-9cc33a9dae38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6a60ec-53f5-40ea-8a65-6add6066a46e",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "db0ad945-eb9f-4b4e-b2a3-adf419b47124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "51a1b4aa-6435-4d34-acb9-166d9953c95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0ad945-eb9f-4b4e-b2a3-adf419b47124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f319f17-4949-42b9-bd6b-0ea1aae683e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0ad945-eb9f-4b4e-b2a3-adf419b47124",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "fb7432c9-c0bd-4c73-ae7b-99ff87aef019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "7d72b870-0434-4f80-872f-ed9f4809fbdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7432c9-c0bd-4c73-ae7b-99ff87aef019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69f1a037-7c0e-4c9d-86dd-7758f1bf2af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7432c9-c0bd-4c73-ae7b-99ff87aef019",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "cfe34933-0713-45b2-b39e-61286a8a07d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "823a5698-0472-4f97-a22d-06159c46f351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe34933-0713-45b2-b39e-61286a8a07d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b47063-1395-4922-8933-edc712b41dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe34933-0713-45b2-b39e-61286a8a07d5",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "d60e4e94-2f1b-4ac2-95dc-cf1bac3f5284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "ab7889ea-3a7b-4112-a1b2-a056e7f18f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60e4e94-2f1b-4ac2-95dc-cf1bac3f5284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2dfbec6-fc9a-4f1c-ac41-4eba68ce09d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60e4e94-2f1b-4ac2-95dc-cf1bac3f5284",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c2b63f76-babe-46cc-a4e8-c17a2acb7171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "f4d51768-0454-4a3b-adb8-1b522144c4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b63f76-babe-46cc-a4e8-c17a2acb7171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e53903-d9bd-4e65-b81e-245453ed5862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b63f76-babe-46cc-a4e8-c17a2acb7171",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c49471d1-ac19-475d-ada4-398599505490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5f72a3a1-0f5a-4287-b666-ec9ca448b7c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49471d1-ac19-475d-ada4-398599505490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdeb8e43-7885-4cf8-b7e6-63dca4263bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49471d1-ac19-475d-ada4-398599505490",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "7e76e3b4-5e97-4077-bc0a-fdb3a351709f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "d6ca2877-6a73-4078-94ad-d45c2f784526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e76e3b4-5e97-4077-bc0a-fdb3a351709f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f2bcc53-47ba-4930-bc0e-5e62b58234a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e76e3b4-5e97-4077-bc0a-fdb3a351709f",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "edfe0cc4-2f40-4182-a27a-441e1d2efc12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "274a05d8-66d6-4efb-b387-b6bf668ed3c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edfe0cc4-2f40-4182-a27a-441e1d2efc12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaab18c5-72cb-44dc-8b06-ca88f3eb04b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edfe0cc4-2f40-4182-a27a-441e1d2efc12",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "b16a9129-9ac1-41ed-9783-f8602fdf86cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3c39ed8f-5c25-4b70-b459-437ed5a7b106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b16a9129-9ac1-41ed-9783-f8602fdf86cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da895c4-4ac6-47d3-883f-5a76dc55cdc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b16a9129-9ac1-41ed-9783-f8602fdf86cb",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "d8ea69e0-f3e7-4800-8857-a90e04bfe6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "9e594db8-c9cd-4572-8ea4-2cc9e791eefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8ea69e0-f3e7-4800-8857-a90e04bfe6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d546f5d-f599-4c44-95e7-45409d8604e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8ea69e0-f3e7-4800-8857-a90e04bfe6e4",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "d2b5757d-a12d-4ebb-a464-b78c1fafa004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "de986fcf-369d-4a85-8002-17aaa8f0c944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b5757d-a12d-4ebb-a464-b78c1fafa004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3683aa84-4a61-4f4c-9058-9ea0488d83be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b5757d-a12d-4ebb-a464-b78c1fafa004",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "fe8f7a83-477f-440b-a330-76d425f62e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "ad492f3a-5b4f-40ee-bc3e-09ba3b3c253c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8f7a83-477f-440b-a330-76d425f62e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd445da9-bfe2-4e35-93d9-caeffe29a213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8f7a83-477f-440b-a330-76d425f62e8e",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c7b681e1-fdc8-45e1-a731-d8ec9dd412b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "00fc5af7-0306-45ea-8d41-e8717cf738b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7b681e1-fdc8-45e1-a731-d8ec9dd412b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b992dfd7-2e22-4304-852e-7372c53f026e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b681e1-fdc8-45e1-a731-d8ec9dd412b1",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "51ed5bde-1deb-4ddd-9139-9ef772340f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "1842ad54-b63f-4a0d-b0ab-3859485cc4b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ed5bde-1deb-4ddd-9139-9ef772340f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6dfee31-6ba2-4fff-acd3-d7b1f90db316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ed5bde-1deb-4ddd-9139-9ef772340f0b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f021db5f-748e-48dc-ba3b-afefd5b3ce50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "e4b4898f-a4f1-47ca-b161-7977801dc6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f021db5f-748e-48dc-ba3b-afefd5b3ce50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997727a0-a8cf-429d-bcca-27da146cd34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f021db5f-748e-48dc-ba3b-afefd5b3ce50",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "5cbbb1d5-2418-469b-864a-38150c6d6206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "04abf336-0609-477b-83d1-260fccae5e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cbbb1d5-2418-469b-864a-38150c6d6206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b523d5-bbc6-457e-9c1c-cd134365836a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cbbb1d5-2418-469b-864a-38150c6d6206",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "25e0ce1d-67d6-4a72-989e-e63673960d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "4ecf55aa-b069-464b-b76b-84274e5f74a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e0ce1d-67d6-4a72-989e-e63673960d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3451d82-81e7-4032-9d32-5d3017f91999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e0ce1d-67d6-4a72-989e-e63673960d65",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "412057ae-af85-42ae-b2e1-5ea42c62bbbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "19b0376b-c0b1-4ddd-993e-4011519d2200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412057ae-af85-42ae-b2e1-5ea42c62bbbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c930ac5-3544-40ad-9280-65d357e05bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412057ae-af85-42ae-b2e1-5ea42c62bbbd",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f0ec3543-1961-484a-8fb5-4a175b4809b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "e13f6934-bd64-417b-b74f-d7e2d78b21c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ec3543-1961-484a-8fb5-4a175b4809b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31001471-84df-4800-be1c-e8ace3685f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ec3543-1961-484a-8fb5-4a175b4809b5",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "19ea6fa2-cafc-4716-b6a6-d6c6d27569b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5f02bfca-2b7c-40b1-a67d-3d898d8dad19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ea6fa2-cafc-4716-b6a6-d6c6d27569b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa31201-7dad-4b4e-954e-958304d9c2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ea6fa2-cafc-4716-b6a6-d6c6d27569b4",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "3a22208e-8518-4d95-a13e-3fa3b8d766de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "d5ffa042-cee3-475d-89ba-80966bad757f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a22208e-8518-4d95-a13e-3fa3b8d766de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f11dc75-0ede-40ac-909c-30a0e6c4aa25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a22208e-8518-4d95-a13e-3fa3b8d766de",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "246a1670-7915-4090-a906-25967337a710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "9c4b15d1-91ad-4b46-abaf-1c5c2d3bcf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246a1670-7915-4090-a906-25967337a710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0d292d-ec69-49e5-b334-94ddc5ee34cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246a1670-7915-4090-a906-25967337a710",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "3bd4903c-a673-474b-b511-e642332afab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3cf7282c-5217-4032-af8f-29e229c71cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd4903c-a673-474b-b511-e642332afab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b18ab36-961a-42da-b184-7ea64b9c15d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd4903c-a673-474b-b511-e642332afab4",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "97fa2fb8-496e-4247-90dc-01fe74df7477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "0ba68014-1301-46e6-8206-082c303c4538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fa2fb8-496e-4247-90dc-01fe74df7477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32722b7-e219-4d68-9dba-901e3a23881e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fa2fb8-496e-4247-90dc-01fe74df7477",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2c895620-3591-44ad-9820-490ee81ddf78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3725c919-984c-42f4-b712-13bf12d9e3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c895620-3591-44ad-9820-490ee81ddf78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8faa21e-3cc0-45d1-a4a2-596aec68529f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c895620-3591-44ad-9820-490ee81ddf78",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "ca703387-9c85-460d-b42d-9e940983570c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "02ecb267-6696-44cf-9c14-bdec771f989c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca703387-9c85-460d-b42d-9e940983570c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01ed454-1722-4785-9a01-b8d8e62a4b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca703387-9c85-460d-b42d-9e940983570c",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "18d2b21f-e578-43d9-a8da-c23c292e8099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5bce04a7-aeff-458b-855a-88df9a66c8c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d2b21f-e578-43d9-a8da-c23c292e8099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d23b304-266a-4c61-aa66-90cfee9248fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d2b21f-e578-43d9-a8da-c23c292e8099",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "cc99a515-0fbc-4497-a3ff-638966402866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "674cc772-5737-4208-9dc4-b099f79fbe23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc99a515-0fbc-4497-a3ff-638966402866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cc52cb-9eac-4fa7-b66b-b08db2a5cb14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc99a515-0fbc-4497-a3ff-638966402866",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f7359bc4-fba2-4d0e-945c-4769aaf0a115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "6804ea70-88c2-4fcd-a1bc-d4fb98b1acc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7359bc4-fba2-4d0e-945c-4769aaf0a115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74800683-11ed-419a-b80e-0cc45646066e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7359bc4-fba2-4d0e-945c-4769aaf0a115",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "e2018553-c64f-4253-893c-39d47333db49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "36800450-abf3-481b-8630-59ac534fc6ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2018553-c64f-4253-893c-39d47333db49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a265e066-dec3-4557-94ee-e0422fce499e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2018553-c64f-4253-893c-39d47333db49",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "592758eb-3dfd-4126-9794-ebc762fbef6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "d67776d6-d771-4775-ab55-d24a5daa8a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592758eb-3dfd-4126-9794-ebc762fbef6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e5d493a-7c60-4356-9e11-065275f187cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592758eb-3dfd-4126-9794-ebc762fbef6d",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "ffd3b065-894b-4ad5-817f-c16169f2d2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3af60b44-f99e-4a3b-8c48-0b8dbd79c5be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd3b065-894b-4ad5-817f-c16169f2d2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6d26d90-a161-4c79-9dc3-215482105e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd3b065-894b-4ad5-817f-c16169f2d2d9",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2c3e2b6d-78c5-431f-b522-d8293683de0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "28c600d3-46b9-4513-880e-e5cb9fbfbf86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3e2b6d-78c5-431f-b522-d8293683de0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02296922-ab96-4c20-8721-9fcd55e3de9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3e2b6d-78c5-431f-b522-d8293683de0e",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "b814d8ff-bdf8-4289-bf4a-2c622e5edd88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "8c4debc2-ddb3-4d18-8bd7-08ab3cb13b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b814d8ff-bdf8-4289-bf4a-2c622e5edd88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afc4571-d4db-4dae-b50c-0c00e4bc0f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b814d8ff-bdf8-4289-bf4a-2c622e5edd88",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "0a9a2c32-d40a-4771-a005-b1daaf8a8757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "37d8cc27-13e3-4ca8-a566-87ff73817769",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a9a2c32-d40a-4771-a005-b1daaf8a8757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337bdd34-9d1e-4099-b223-c6ed17c72316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a9a2c32-d40a-4771-a005-b1daaf8a8757",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "76442554-f4f5-4bf8-b140-d9281041a8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "489269b6-16b3-4cdf-9ef3-43c74b1540fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76442554-f4f5-4bf8-b140-d9281041a8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9074e445-981d-4fb6-8155-3ebdde132d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76442554-f4f5-4bf8-b140-d9281041a8b7",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "aef1341f-d2ad-4baf-ab13-8758c1b318a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "c22db486-3821-4ed3-98f6-4f14e6ed25d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef1341f-d2ad-4baf-ab13-8758c1b318a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade845f2-8891-43ac-aae9-567ef29c1eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef1341f-d2ad-4baf-ab13-8758c1b318a8",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "1b295a8a-77d5-48ff-beb4-84f72de74498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "fec4fc30-241a-4230-bdc9-5a2d3352bb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b295a8a-77d5-48ff-beb4-84f72de74498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a281064-7e0b-4326-81f6-a1ea6931522e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b295a8a-77d5-48ff-beb4-84f72de74498",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "b66e882e-0e96-43dd-8782-3a5127a9d5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "e5b28ca1-e33a-456b-9b85-90ee7d97f428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b66e882e-0e96-43dd-8782-3a5127a9d5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34594b2-19db-4d72-a47d-6df2cdf7da71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b66e882e-0e96-43dd-8782-3a5127a9d5f4",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "4194469d-4f4c-4d00-833a-f411f79fa3af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "938cef72-7657-4bcb-9cf2-f64fccb0f464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4194469d-4f4c-4d00-833a-f411f79fa3af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2846fcd4-3799-4a00-a027-f4173f980843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4194469d-4f4c-4d00-833a-f411f79fa3af",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c0deaec3-3a0a-4c50-bef6-17dc782442f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "b62c2889-f4c0-49ad-8d1e-aa3d3e7738c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0deaec3-3a0a-4c50-bef6-17dc782442f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990012fe-1160-4bcc-834c-fe4223d03806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0deaec3-3a0a-4c50-bef6-17dc782442f3",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "01ea7b71-d9bd-403d-9e3c-48ade2d35a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "2b5b5e91-3883-4914-91fb-9abf51a50168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ea7b71-d9bd-403d-9e3c-48ade2d35a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fc0d64-3a5b-46ae-89f6-be3d1be7ceb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ea7b71-d9bd-403d-9e3c-48ade2d35a2d",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "8d440918-080e-49bb-bef0-48cd0efebdac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "8b6cb836-558e-420f-a2fa-625e0b530f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d440918-080e-49bb-bef0-48cd0efebdac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5011c8d4-9482-483b-8c65-d443c4a8f979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d440918-080e-49bb-bef0-48cd0efebdac",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "96657207-45a2-4c39-8d98-7ca0d67aa035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "6595d9f3-fb7c-4e87-83a6-ef04dbb6e13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96657207-45a2-4c39-8d98-7ca0d67aa035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a828643a-2049-4646-90e3-fd8377ad6eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96657207-45a2-4c39-8d98-7ca0d67aa035",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "7f73d474-e141-4011-b851-bb478e785d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "0d7cfd51-5b80-481e-99dc-e1dfda277fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f73d474-e141-4011-b851-bb478e785d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d789281a-6792-46e6-97ac-49bda4f149de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f73d474-e141-4011-b851-bb478e785d3e",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "87448ca9-be39-4c04-a605-21f0ca7c1f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "570c5c5e-caea-484e-b7c5-f9be5571a0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87448ca9-be39-4c04-a605-21f0ca7c1f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff31aee3-15aa-45f9-8f78-2ace7fb0b953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87448ca9-be39-4c04-a605-21f0ca7c1f57",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "ee449d4c-7408-40a6-abd4-951629ec2437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "bb7b8a33-58e6-424e-bdd4-bfdad59f6f82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee449d4c-7408-40a6-abd4-951629ec2437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce2efc6-1ab9-4d67-ac01-9c7a765b032b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee449d4c-7408-40a6-abd4-951629ec2437",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "bfa1d1af-2d7d-4766-b5cb-97342a0df8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "621abed5-915e-43e1-bb91-74a99754f5f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa1d1af-2d7d-4766-b5cb-97342a0df8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd0ca5e-b185-42df-a506-a86b8d56b1f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa1d1af-2d7d-4766-b5cb-97342a0df8c7",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "ed65436a-aa72-415e-950b-9d895e42f5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "153667ef-a552-42aa-8baa-b5dfe8ac7d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed65436a-aa72-415e-950b-9d895e42f5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb20d647-2d86-44c7-bde2-748a70ac4176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed65436a-aa72-415e-950b-9d895e42f5f2",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "a193141c-dc39-48da-a69e-acd9ab8bd20c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "9d1b3d46-83f5-4558-aab1-8f47cb2f0427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a193141c-dc39-48da-a69e-acd9ab8bd20c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352409b9-a390-4d33-9f33-4a7e3e5fa2f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a193141c-dc39-48da-a69e-acd9ab8bd20c",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "3094ce5a-9cfb-43b7-ae94-7db5e62d4f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "16ffbb4d-25e6-4cf5-b70e-4ef43736a6e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3094ce5a-9cfb-43b7-ae94-7db5e62d4f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c347f0e4-9b46-4d4c-8e87-16c806495da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3094ce5a-9cfb-43b7-ae94-7db5e62d4f7b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f5993234-2ddf-4757-8a7a-453733032404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "93f56225-3f5d-46e6-8ee0-672d25fd2237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5993234-2ddf-4757-8a7a-453733032404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d386e03-26eb-43b6-bba6-07885170e2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5993234-2ddf-4757-8a7a-453733032404",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c1322705-4e46-42fa-bb83-4e206da95516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "8116c907-8dee-4b32-b6ca-1c3ab1e692cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1322705-4e46-42fa-bb83-4e206da95516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f209bb4-785b-49d4-bd7b-87263dc4c70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1322705-4e46-42fa-bb83-4e206da95516",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "97579fd3-4f14-4be1-92f5-0ecedd50929b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "14ba2e14-1747-4d3a-b3ea-c1de9650303f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97579fd3-4f14-4be1-92f5-0ecedd50929b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee4e01b0-fe8c-4bf1-8b28-6bd9ba0802a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97579fd3-4f14-4be1-92f5-0ecedd50929b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "162cfbe1-3f52-414a-9841-dadbb1faf0d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "353cebde-0fd5-4b06-9f9b-6c21aa5c8e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "162cfbe1-3f52-414a-9841-dadbb1faf0d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832c638e-e4a6-456b-b1e7-869671b4802a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "162cfbe1-3f52-414a-9841-dadbb1faf0d6",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "86a08ae3-a0d1-45e1-88f0-2301a3401389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "9572ef24-9a53-43d5-a188-159e37b86b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86a08ae3-a0d1-45e1-88f0-2301a3401389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83dc2e5-6265-4883-ab01-c7ac7256e89e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86a08ae3-a0d1-45e1-88f0-2301a3401389",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "ed2ccb91-bda4-43f0-9f77-f293b6def8e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "c29419ed-4f01-4f3c-95a0-785053258017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2ccb91-bda4-43f0-9f77-f293b6def8e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bacf7fd2-7624-48dc-80dd-94025a439c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2ccb91-bda4-43f0-9f77-f293b6def8e0",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "7fd9ccba-cd58-4de9-b724-bd7d67ecc5e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "44df3c04-d88c-49ac-a6a5-eaa33a052cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd9ccba-cd58-4de9-b724-bd7d67ecc5e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad81282-37d4-4473-a447-91fc746c0eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd9ccba-cd58-4de9-b724-bd7d67ecc5e7",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "22973a63-b49e-4daa-bfe2-6036848d1567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "f27b8581-3793-4d72-9b63-b7803bbef08e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22973a63-b49e-4daa-bfe2-6036848d1567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59a42596-eb09-4258-914e-500eb713f121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22973a63-b49e-4daa-bfe2-6036848d1567",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "447269f3-47c1-4f53-b6cf-5f19ed9480e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "46fea13a-7e42-44b5-9d97-b6f9ee0b43a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "447269f3-47c1-4f53-b6cf-5f19ed9480e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63abdbef-88f7-4809-9787-f0e68eb24fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "447269f3-47c1-4f53-b6cf-5f19ed9480e8",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "b1901d4a-65b6-49f4-b04b-f3bcc154273b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "a75e09b2-dd1a-4552-8e5e-f1375c0c6592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1901d4a-65b6-49f4-b04b-f3bcc154273b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aba1e4d-181c-48d2-8ae2-34311d18227b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1901d4a-65b6-49f4-b04b-f3bcc154273b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "a6f56533-88f2-433f-9952-885f4325c290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "b0b05a45-204c-480f-bfb3-3c7dda4b3f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f56533-88f2-433f-9952-885f4325c290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5670d01e-702b-4ec5-aad2-e765156d46e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f56533-88f2-433f-9952-885f4325c290",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "972e88c2-13bc-4834-aa6f-b2369f85bbce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "cb5e6a79-c463-408a-9270-b4385f327913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972e88c2-13bc-4834-aa6f-b2369f85bbce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5e59ad-dd05-4e53-adee-fc762805de0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972e88c2-13bc-4834-aa6f-b2369f85bbce",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "51a43a88-6214-4c92-9a9c-ecb15c068d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "498a2937-3b58-4b97-aaa8-0a360a07d715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a43a88-6214-4c92-9a9c-ecb15c068d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c332fe3-d36e-4894-9b47-670ba9d7892a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a43a88-6214-4c92-9a9c-ecb15c068d3b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "193c616e-6e78-4ad5-a5f1-378c0a056645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "a7d98606-c015-4c64-aa1d-a2c3ca6f950f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193c616e-6e78-4ad5-a5f1-378c0a056645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b1c7ca-8403-4b23-b49b-2b99453dc8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193c616e-6e78-4ad5-a5f1-378c0a056645",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "84cb1036-ebb3-4838-80d8-ea622ac3f0cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "4ece1b16-517e-4004-9783-d11166c9549d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84cb1036-ebb3-4838-80d8-ea622ac3f0cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "986e3dad-4ca4-4ef0-a19d-28b0a2f35a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84cb1036-ebb3-4838-80d8-ea622ac3f0cc",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2456b263-18d6-4c7c-bd37-2b1fab5321c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "a01cc6d7-a9ae-4776-96bb-d79b64971cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2456b263-18d6-4c7c-bd37-2b1fab5321c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2487b3c9-73a0-44e0-a9ab-e939eeee5e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2456b263-18d6-4c7c-bd37-2b1fab5321c8",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "020eae49-2f6e-40a6-882e-235b67a8d5a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "dc227eb0-5245-405c-8658-c9918af2b841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "020eae49-2f6e-40a6-882e-235b67a8d5a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06c4a70-a5ad-46c9-9dc6-91650f8be769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "020eae49-2f6e-40a6-882e-235b67a8d5a5",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "bb08bcf1-7492-4bb1-9220-204877a20699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3203ccfa-d85f-46af-8d3d-a1a3b988eb86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb08bcf1-7492-4bb1-9220-204877a20699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5608ad1c-52b3-4418-ae09-ea2596a678bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb08bcf1-7492-4bb1-9220-204877a20699",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "4133b4a7-c7ad-47ca-8dcc-07a4555ff3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5b0ce69b-14d6-4aff-8e6b-8575cc3a7902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4133b4a7-c7ad-47ca-8dcc-07a4555ff3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac2c0db6-2f9d-41ab-8a8e-f0952ca528c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4133b4a7-c7ad-47ca-8dcc-07a4555ff3b7",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "9420445c-90c9-426f-9231-6e0bc6fc0b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "dda12dde-fc77-43d2-86a8-81b5f4aa703b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9420445c-90c9-426f-9231-6e0bc6fc0b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed61af2-129d-4c9f-aab7-2810f81c0c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9420445c-90c9-426f-9231-6e0bc6fc0b66",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f2781b5a-36f3-450d-abbe-849c8b2c9c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5634b41c-e29a-4471-8d6f-533e86fa84c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2781b5a-36f3-450d-abbe-849c8b2c9c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f414d0-975e-4f9b-a25a-e43ef399e85b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2781b5a-36f3-450d-abbe-849c8b2c9c16",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "19f469a1-fd96-4a5a-a782-a659259d2dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "7aa19b43-8f1b-444b-b310-6456502ac419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f469a1-fd96-4a5a-a782-a659259d2dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ced7f85-53cb-4359-a182-c5d0b4a8a774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f469a1-fd96-4a5a-a782-a659259d2dd0",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "9de4b745-053b-4572-88fb-5e4219242763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "d2ef2672-7e17-4e1e-9ae3-5d352ee210b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de4b745-053b-4572-88fb-5e4219242763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07627d28-d0d2-48a8-a6dd-d8c1447a6967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de4b745-053b-4572-88fb-5e4219242763",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2665e6bf-a864-4915-9f99-ae222d068e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "6c7ae041-f497-4755-a35a-713fa123209f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2665e6bf-a864-4915-9f99-ae222d068e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a51021-201c-4c43-aaba-1720c30844eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2665e6bf-a864-4915-9f99-ae222d068e72",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "a56b3b6b-6301-436a-9906-54271bc07113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "d9d40e14-e5d6-4bcc-a226-e0879c16f296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56b3b6b-6301-436a-9906-54271bc07113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37dede15-eec6-4415-bbf0-1f239b5ee2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56b3b6b-6301-436a-9906-54271bc07113",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "e8a58f0c-3560-40b7-9797-12f21cee8728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "30b0435f-5009-4ed3-a4ea-eb167da2d255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a58f0c-3560-40b7-9797-12f21cee8728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb3a984-0026-400c-96e9-c0e3a22b264b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a58f0c-3560-40b7-9797-12f21cee8728",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "96507eb9-10b7-44b3-8f7f-bde455b14a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "c8cb27ff-f8d1-49de-8c52-ea40f6e20644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96507eb9-10b7-44b3-8f7f-bde455b14a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab37992f-8c2d-4da6-8a84-166912f343dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96507eb9-10b7-44b3-8f7f-bde455b14a11",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "9654e2ab-92d2-49ed-92a3-f50fb4ec5049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "94c77fe8-f5e9-4678-bbfe-708d1ed923a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9654e2ab-92d2-49ed-92a3-f50fb4ec5049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18cc2a71-f8d0-4766-81ce-9a948dc55117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9654e2ab-92d2-49ed-92a3-f50fb4ec5049",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2d257cfb-2aab-4eff-ad9d-9eecb4edf800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "99184b3d-0c28-4f6f-b57c-9ebe1271caec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d257cfb-2aab-4eff-ad9d-9eecb4edf800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd5f700-2ed3-4705-9d20-83751de774b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d257cfb-2aab-4eff-ad9d-9eecb4edf800",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "d4f6acdc-c031-4327-aaf4-b8d21eaf502c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "57a1878b-638b-4603-8a9d-4c1f4302fccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f6acdc-c031-4327-aaf4-b8d21eaf502c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ab48a2-6efa-4ae3-b075-585ca4d89436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f6acdc-c031-4327-aaf4-b8d21eaf502c",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "1e29c8e8-7186-4e52-b264-df40d8bf8d29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "68266d35-3b7f-4887-a46d-469797460b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e29c8e8-7186-4e52-b264-df40d8bf8d29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e7d099-f783-4438-8c0e-82e44a743421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e29c8e8-7186-4e52-b264-df40d8bf8d29",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "d0a72a93-9a74-48c2-833f-4719a281287f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "7130d071-cc00-423b-8d16-1015e3c3f74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a72a93-9a74-48c2-833f-4719a281287f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22720011-0cfb-47f7-a4a5-a3df09e6f404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a72a93-9a74-48c2-833f-4719a281287f",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "2ce73d92-3e0c-42e5-9d90-4a7e56628931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "3a74e9c7-1765-42c7-8ae8-50800e309a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce73d92-3e0c-42e5-9d90-4a7e56628931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10034e3-d17a-452d-afa7-f973973119b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce73d92-3e0c-42e5-9d90-4a7e56628931",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "5d7a8eb9-eb7c-4e38-9c29-bcd52bd781cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "0d415f66-9374-4622-b637-86eaaa17e569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7a8eb9-eb7c-4e38-9c29-bcd52bd781cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a6e5ab-0f30-47ae-a23d-da648f6453d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7a8eb9-eb7c-4e38-9c29-bcd52bd781cb",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "a8697733-a485-46a7-a2bb-396d6899579b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "10f132fd-dabd-4909-8dd7-159347443ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8697733-a485-46a7-a2bb-396d6899579b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05658f25-b424-4626-9842-817745ec28ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8697733-a485-46a7-a2bb-396d6899579b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "a23ef9ca-a045-418e-b096-e10e874a7466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "215966b3-cb90-427e-893f-902c6d90fb2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a23ef9ca-a045-418e-b096-e10e874a7466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718fd323-d4e0-4883-b02e-381b7f2b5a86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a23ef9ca-a045-418e-b096-e10e874a7466",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "9261f1b6-f465-42c7-aff8-0ed1ee449b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "97eee7b2-18c2-4c31-b875-aa78bd248490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9261f1b6-f465-42c7-aff8-0ed1ee449b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c8346c-1e5f-47de-b6c9-b37b4fd62df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9261f1b6-f465-42c7-aff8-0ed1ee449b0b",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "1861c443-c2bc-4b12-8456-c617c5fe61ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "6bcb4541-5c62-4d14-8ce4-27567432cb0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1861c443-c2bc-4b12-8456-c617c5fe61ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "372cb2c7-5bd3-4368-851c-e22c6d6a6362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1861c443-c2bc-4b12-8456-c617c5fe61ff",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f0a77f15-000d-416d-8ab7-cf9f2c9c3642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "72fe2d85-343e-402f-a1b8-850684b37936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0a77f15-000d-416d-8ab7-cf9f2c9c3642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f95ba397-ab92-43eb-ac5f-885a92f49fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0a77f15-000d-416d-8ab7-cf9f2c9c3642",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "e22d1df3-74b4-45aa-ae4b-f6c84fa5a3d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "595d74e4-48e7-4f35-af7a-c2a3d18c8819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22d1df3-74b4-45aa-ae4b-f6c84fa5a3d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2e2434-29c6-4e98-8bdf-95cd79e23097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22d1df3-74b4-45aa-ae4b-f6c84fa5a3d6",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "0ec74162-6524-4046-ad83-85e10a9ad9b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "e313b6ca-98e6-4e6f-9d39-1b4ded106a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec74162-6524-4046-ad83-85e10a9ad9b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563ba95b-11b4-4f2d-b831-f1c7182087e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec74162-6524-4046-ad83-85e10a9ad9b9",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "f5274ef5-e4a6-4cf5-a26a-f6fd550b960f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "62f8a7f1-80df-4bee-9f97-b7bc92a722cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5274ef5-e4a6-4cf5-a26a-f6fd550b960f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ddc53ee-df40-48f5-9608-f6aaf799b6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5274ef5-e4a6-4cf5-a26a-f6fd550b960f",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "c643b52c-e752-4276-b8e6-e294aa590a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "480bf931-d312-4f25-b631-efa931e1cfdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c643b52c-e752-4276-b8e6-e294aa590a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc03246-7b63-42b8-b7d4-ab0c726d85a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c643b52c-e752-4276-b8e6-e294aa590a32",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "69a0babd-c7e1-4c43-90fb-10627263967e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "ebfe029e-c407-4e39-acea-ca7d59b20976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a0babd-c7e1-4c43-90fb-10627263967e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd5f133-428e-4fd9-919e-7e45c12365da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a0babd-c7e1-4c43-90fb-10627263967e",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "e6faf98b-b89b-4806-959f-d7c9fa98c66a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5004cebe-497c-4981-ab42-848fd153b67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6faf98b-b89b-4806-959f-d7c9fa98c66a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31a11331-bf18-45ca-a0f2-b2cdb686837b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6faf98b-b89b-4806-959f-d7c9fa98c66a",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "bd9a197b-8d14-4d6b-8797-62b8f78bb4b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "fbd3405d-6d9c-4581-893c-bb370b9ee1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9a197b-8d14-4d6b-8797-62b8f78bb4b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8cb17f7-2600-49de-beec-9f7d7fdbf9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9a197b-8d14-4d6b-8797-62b8f78bb4b5",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        },
        {
            "id": "4f54455f-5a2e-4cce-b1f3-2df3599d677d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "compositeImage": {
                "id": "5dc56bf4-e966-478a-b4e8-7cfd91a75bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f54455f-5a2e-4cce-b1f3-2df3599d677d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc3945e-3a2f-46f2-aa20-720b5ce4543c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f54455f-5a2e-4cce-b1f3-2df3599d677d",
                    "LayerId": "ea215878-f348-4e63-a56c-b68e5c32e9f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ea215878-f348-4e63-a56c-b68e5c32e9f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3deded34-d8e9-42a6-ae39-ddf667042894",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}