{
    "id": "32a0b22c-a412-4028-90e2-58c293b08971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadLStickBase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 12,
    "bbox_right": 25,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81818a28-e460-4d70-9614-3dedd6d3c11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32a0b22c-a412-4028-90e2-58c293b08971",
            "compositeImage": {
                "id": "87207754-f2d0-409a-b492-858d6255fc0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81818a28-e460-4d70-9614-3dedd6d3c11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abde3e6a-843f-4e18-b3b1-6b0ec7656ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81818a28-e460-4d70-9614-3dedd6d3c11d",
                    "LayerId": "8df9f4b7-78ae-48dd-b5e6-8515e255d0f7"
                },
                {
                    "id": "1f890c13-642f-422e-bc5b-43083321bdc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81818a28-e460-4d70-9614-3dedd6d3c11d",
                    "LayerId": "a1881737-62d5-4456-95e3-7ba94d82e8ca"
                }
            ]
        },
        {
            "id": "dc1b07c9-6364-4bd0-819e-a6de72cd0718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32a0b22c-a412-4028-90e2-58c293b08971",
            "compositeImage": {
                "id": "6e707649-dde4-461c-b7a6-9963aad1ee2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1b07c9-6364-4bd0-819e-a6de72cd0718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5829f00-b01e-4733-9dd7-963e5ddcd268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1b07c9-6364-4bd0-819e-a6de72cd0718",
                    "LayerId": "8df9f4b7-78ae-48dd-b5e6-8515e255d0f7"
                },
                {
                    "id": "871281fa-4939-4a29-9f29-cce79c33abf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1b07c9-6364-4bd0-819e-a6de72cd0718",
                    "LayerId": "a1881737-62d5-4456-95e3-7ba94d82e8ca"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "8df9f4b7-78ae-48dd-b5e6-8515e255d0f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32a0b22c-a412-4028-90e2-58c293b08971",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a1881737-62d5-4456-95e3-7ba94d82e8ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32a0b22c-a412-4028-90e2-58c293b08971",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}