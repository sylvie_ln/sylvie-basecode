{
    "id": "757a51e3-95bf-43dd-8898-3f279e751af5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigLStick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0fa1dac-3a80-4a07-b100-f3e339af0337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757a51e3-95bf-43dd-8898-3f279e751af5",
            "compositeImage": {
                "id": "33df96e9-7cc1-4203-9380-801e44cca403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fa1dac-3a80-4a07-b100-f3e339af0337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22546ddb-f44b-4a25-8060-df1c0f152b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fa1dac-3a80-4a07-b100-f3e339af0337",
                    "LayerId": "56166155-1aea-4a0b-8025-79bd6362b441"
                },
                {
                    "id": "6dd2983a-e6e0-43f5-ba5b-4aadf9748117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fa1dac-3a80-4a07-b100-f3e339af0337",
                    "LayerId": "68d285f4-0e61-49ea-a86e-691e1487d425"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 14,
    "layers": [
        {
            "id": "56166155-1aea-4a0b-8025-79bd6362b441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "757a51e3-95bf-43dd-8898-3f279e751af5",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "68d285f4-0e61-49ea-a86e-691e1487d425",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "757a51e3-95bf-43dd-8898-3f279e751af5",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}